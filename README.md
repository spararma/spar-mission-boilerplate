# SPAR Tactical Unit Mission Boilerplate  #

Here you can find files that are commonly used in the SPAR Tactical Unit.

### How do I use it? ###

Download the repository and move all the files from here into your mission folder. You can then edit the file to your liking. Don't copy the .git folder, as well as .gitignore or README.md files, they are redundant and they will only make the mission size larger.

### Where do I start? ###

`description.ext` is a great place to start.

Some of the fields you might need to know:

* `onLoadName` is the mission name showing on the loading screen
* `onLoadIntro` is a small description on the loading screen
* `LoadScreen` is a path to an image that shows up on the loading screen

You can visit Bohemia Interactive documentation for more detailed reference:
https://community.bistudio.com/wiki/Description.ext

### What do other files do? ###

`onPlayerKilled.sqf` and `onPlayerRespawn.sqf` are necessary if you want your players to respawn with the gear that they had on them when they died (standard in SPAR).

`TFAR_Freq.sqf` and `TFAR_Freq_Rhino.sqf` are two files which set the short range and long range radio frequencies.

`images` folder contains most commonly used image files - mostly labels and doorsigns, but also "Standby" screen, etc.

`scripts` folder contains other common scripts that we use - mostly related to the SPAR Headquarters.

### Why is my Loadouts folder empty? ###

Because you should get the latest pack of SPAR Loadouts from the SPAR Loadouts repository and then paste them there. Here's the link:

https://bitbucket.org/spararma/spar-loadouts/src/master/

### I've done all of the above. Is the mission ready now? ###

There are still a couple of steps to make the mission ready to play.

* In the 3DEN Editor, go to the 'Attributes' menu on the top and select 'Multiplayer'.
* Set the Game Mode as 'COOP' or similar.
* Set the min players to 1, max to 81
* In the 'Respawn' category, select Respawn to 'Respawn on Custom Position'
* In the section below, check "Select Respawn Position" - it will allow players to choose one of the respawn positions you've placed on the map as modules
* Set 'Respawn Delay' to the time it should take to respawn. It's usually between 1 and 3 minutes in SPAR.

Now it's time to set the correct CBA settings for the mission.

* In the 3DEN Editor, go to 'Settings' menu on the top and select 'Addon Options'
* A Dialog window will pop up - make sure the 'Mission' tab is selected
* Click 'Import' in the bottom-right corner
* Copy the below and paste it in the window, then press OK.

```
// ACE Advanced Ballistics
force ace_advanced_ballistics_ammoTemperatureEnabled = true;
force ace_advanced_ballistics_barrelLengthInfluenceEnabled = true;
force ace_advanced_ballistics_bulletTraceEnabled = true;
force ace_advanced_ballistics_enabled = true;
force ace_advanced_ballistics_muzzleVelocityVariationEnabled = true;
force ace_advanced_ballistics_simulationInterval = 0.05;

// ACE Advanced Fatigue
force ace_advanced_fatigue_enabled = true;
force ace_advanced_fatigue_enableStaminaBar = true;
ace_advanced_fatigue_fadeStaminaBar = true;
force ace_advanced_fatigue_loadFactor = 0.790936;
force ace_advanced_fatigue_performanceFactor = 3.47222;
force ace_advanced_fatigue_recoveryFactor = 4.51245;
force ace_advanced_fatigue_swayFactor = 0.84474;
force ace_advanced_fatigue_terrainGradientFactor = 0.817836;

// ACE Advanced Throwing
ace_advanced_throwing_enabled = true;
force ace_advanced_throwing_enablePickUp = true;
force ace_advanced_throwing_enablePickUpAttached = true;
ace_advanced_throwing_showMouseControls = true;
ace_advanced_throwing_showThrowArc = true;

// ACE Advanced Vehicle Damage
ace_vehicle_damage_enableCarDamage = false;
ace_vehicle_damage_enabled = true;
ace_vehicle_damage_removeAmmoDuringCookoff = true;

// ACE Arsenal
force ace_arsenal_allowDefaultLoadouts = true;
force ace_arsenal_allowSharedLoadouts = true;
ace_arsenal_camInverted = false;
force ace_arsenal_enableIdentityTabs = true;
ace_arsenal_enableModIcons = true;
ace_arsenal_EnableRPTLog = false;
ace_arsenal_fontHeight = 4.5;

// ACE Artillery
force ace_artillerytables_advancedCorrections = false;
force ace_artillerytables_disableArtilleryComputer = false;
force ace_mk6mortar_airResistanceEnabled = false;
force ace_mk6mortar_allowCompass = true;
force ace_mk6mortar_allowComputerRangefinder = true;
force ace_mk6mortar_useAmmoHandling = false;

// ACE Captives
force ace_captives_allowHandcuffOwnSide = true;
force ace_captives_allowSurrender = true;
force ace_captives_requireSurrender = 1;
force ace_captives_requireSurrenderAi = false;

// ACE Common
force ace_common_allowFadeMusic = true;
force ace_common_checkPBOsAction = 0;
force ace_common_checkPBOsCheckAll = false;
force ace_common_checkPBOsWhitelist = "[]";
ace_common_displayTextColor = [0,0,0,0.1];
ace_common_displayTextFontColor = [1,1,1,1];
ace_common_epilepsyFriendlyMode = false;
ace_common_progressBarInfo = 2;
ace_common_settingFeedbackIcons = 1;
ace_common_settingProgressBarLocation = 0;
force ace_noradio_enabled = true;

// ACE Cook off
force ace_cookoff_ammoCookoffDuration = 1;
force ace_cookoff_enable = 0;
force ace_cookoff_enableAmmobox = true;
force ace_cookoff_enableAmmoCookoff = true;
ace_cookoff_enableFire = true;
force ace_cookoff_probabilityCoef = 1;

// ACE Crew Served Weapons
force ace_csw_ammoHandling = 2;
force ace_csw_defaultAssemblyMode = false;
ace_csw_dragAfterDeploy = false;
force ace_csw_handleExtraMagazines = true;
force ace_csw_progressBarTimeCoefficent = 1;

// ACE Dragging
ace_dragging_dragAndFire = true;

// ACE Explosives
ace_explosives_customTimerDefault = 30;
force ace_explosives_customTimerMax = 900;
force ace_explosives_customTimerMin = 5;
force ace_explosives_explodeOnDefuse = true;
force ace_explosives_punishNonSpecialists = true;
force ace_explosives_requireSpecialist = false;

// ACE Field Rations
force acex_field_rations_affectAdvancedFatigue = true;
force acex_field_rations_enabled = false;
acex_field_rations_hudShowLevel = 0;
acex_field_rations_hudTransparency = -1;
acex_field_rations_hudType = 0;
force acex_field_rations_hungerSatiated = 1;
force acex_field_rations_terrainObjectActions = true;
force acex_field_rations_thirstQuenched = 1;
force acex_field_rations_timeWithoutFood = 2;
force acex_field_rations_timeWithoutWater = 2;
force acex_field_rations_waterSourceActions = 2;

// ACE Fire
ace_fire_dropWeapon = 1;
ace_fire_enabled = true;
ace_fire_enableFlare = false;
ace_fire_enableScreams = true;

// ACE Fortify
ace_fortify_markObjectsOnMap = 1;
ace_fortify_timeCostCoefficient = 1;
ace_fortify_timeMin = 1.5;
acex_fortify_settingHint = 2;

// ACE Fragmentation Simulation
force ace_frag_enabled = true;
force ace_frag_maxTrack = 10;
force ace_frag_maxTrackPerFrame = 10;
force ace_frag_reflectionsEnabled = false;
force ace_frag_spallEnabled = false;

// ACE G-Forces
force ace_gforces_coef = 0.802054;
force ace_gforces_enabledFor = 1;

// ACE Goggles
ace_goggles_effects = 2;
ace_goggles_showClearGlasses = false;
ace_goggles_showInThirdPerson = false;

// ACE Grenades
force ace_grenades_convertExplosives = true;

// ACE Headless
force acex_headless_delay = 15;
force acex_headless_enabled = false;
force acex_headless_endMission = 0;
force acex_headless_log = false;
force acex_headless_transferLoadout = 0;

// ACE Hearing
force ace_hearing_autoAddEarplugsToUnits = true;
ace_hearing_disableEarRinging = false;
force ace_hearing_earplugsVolume = 0.5;
force ace_hearing_enableCombatDeafness = true;
force ace_hearing_enabledForZeusUnits = false;
force ace_hearing_unconsciousnessVolume = 0.4;

// ACE Interaction
force ace_interaction_disableNegativeRating = false;
ace_interaction_enableGroupRenaming = true;
ace_interaction_enableMagazinePassing = true;
force ace_interaction_enableTeamManagement = true;
ace_interaction_enableWeaponAttachments = true;
ace_interaction_interactWithTerrainObjects = false;

// ACE Interaction Menu
ace_gestures_showOnInteractionMenu = 2;
ace_interact_menu_actionOnKeyRelease = true;
ace_interact_menu_addBuildingActions = false;
ace_interact_menu_alwaysUseCursorInteraction = false;
ace_interact_menu_alwaysUseCursorSelfInteraction = true;
ace_interact_menu_colorShadowMax = [0,0,0,1];
ace_interact_menu_colorShadowMin = [0,0,0,0.25];
ace_interact_menu_colorTextMax = [1,1,1,1];
ace_interact_menu_colorTextMin = [1,1,1,0.25];
ace_interact_menu_consolidateSingleChild = false;
ace_interact_menu_cursorKeepCentered = false;
ace_interact_menu_cursorKeepCenteredSelfInteraction = false;
ace_interact_menu_menuAnimationSpeed = 0;
ace_interact_menu_menuBackground = 0;
ace_interact_menu_menuBackgroundSelf = 0;
ace_interact_menu_selectorColor = [1,0,0];
ace_interact_menu_shadowSetting = 2;
ace_interact_menu_textSize = 2;
ace_interact_menu_useListMenu = true;
ace_interact_menu_useListMenuSelf = false;

// ACE Logistics
force ace_cargo_enable = true;
ace_cargo_enableRename = true;
force ace_cargo_loadTimeCoefficient = 5;
ace_cargo_openAfterUnload = 0;
force ace_cargo_paradropTimeCoefficent = 2.5;
force ace_rearm_distance = 20;
force ace_rearm_level = 0;
force ace_rearm_supply = 0;
force ace_refuel_hoseLength = 12;
force ace_refuel_rate = 1;
force ace_repair_addSpareParts = true;
force ace_repair_autoShutOffEngineWhenStartingRepair = false;
force ace_repair_consumeItem_toolKit = 0;
ace_repair_displayTextOnRepair = true;
force ace_repair_engineerSetting_fullRepair = 2;
force ace_repair_engineerSetting_repair = 1;
force ace_repair_engineerSetting_wheel = 0;
force ace_repair_fullRepairLocation = 2;
ace_repair_fullRepairRequiredItems = ["ace_repair_anyToolKit"];
ace_repair_locationsBoostTraining = false;
ace_repair_miscRepairRequiredItems = ["ace_repair_anyToolKit"];
force ace_repair_repairDamageThreshold = 0.6;
force ace_repair_repairDamageThreshold_engineer = 0.4;
force ace_repair_wheelRepairRequiredItems = [];

// ACE Magazine Repack
ace_magazinerepack_repackLoadedMagazines = true;
force ace_magazinerepack_timePerAmmo = 1.5;
force ace_magazinerepack_timePerBeltLink = 8;
force ace_magazinerepack_timePerMagazine = 2;

// ACE Map
force ace_map_BFT_Enabled = false;
force ace_map_BFT_HideAiGroups = false;
force ace_map_BFT_Interval = 1;
force ace_map_BFT_ShowPlayerNames = false;
force ace_map_DefaultChannel = -1;
force ace_map_mapGlow = true;
force ace_map_mapIllumination = true;
force ace_map_mapLimitZoom = false;
force ace_map_mapShake = true;
force ace_map_mapShowCursorCoordinates = false;
ace_markers_moveRestriction = 0;
ace_markers_timestampEnabled = true;
ace_markers_timestampFormat = "HH:MM";
ace_markers_timestampHourFormat = 24;

// ACE Map Gestures
ace_map_gestures_allowCurator = true;
ace_map_gestures_allowSpectator = true;
ace_map_gestures_briefingMode = 0;
ace_map_gestures_defaultColor = [1,0.88,0,0.7];
ace_map_gestures_defaultLeadColor = [1,0.88,0,0.95];
force ace_map_gestures_enabled = true;
force ace_map_gestures_interval = 0.03;
force ace_map_gestures_maxRange = 7;
force ace_map_gestures_maxRangeCamera = 14;
ace_map_gestures_nameTextColor = [0.2,0.2,0.2,0.3];
force ace_map_gestures_onlyShowFriendlys = false;

// ACE Map Tools
ace_maptools_drawStraightLines = true;
ace_maptools_rotateModifierKey = 1;

// ACE Medical
force ace_medical_ai_enabledFor = 2;
force ace_medical_AIDamageThreshold = 1;
force ace_medical_bleedingCoefficient = 1;
force ace_medical_blood_bloodLifetime = 900;
force ace_medical_blood_enabledFor = 2;
force ace_medical_blood_maxBloodObjects = 500;
ace_medical_deathChance = 1;
ace_medical_enableVehicleCrashes = true;
force ace_medical_fatalDamageSource = 1;
force ace_medical_feedback_bloodVolumeEffectType = 0;
ace_medical_feedback_enableHUDIndicators = true;
force ace_medical_feedback_painEffectType = 0;
force ace_medical_fractureChance = 0.8;
force ace_medical_fractures = 2;
ace_medical_gui_bloodLossColor_0 = [1,1,1,1];
ace_medical_gui_bloodLossColor_1 = [1,0.95,0.64,1];
ace_medical_gui_bloodLossColor_2 = [1,0.87,0.46,1];
ace_medical_gui_bloodLossColor_3 = [1,0.8,0.33,1];
ace_medical_gui_bloodLossColor_4 = [1,0.72,0.24,1];
ace_medical_gui_bloodLossColor_5 = [1,0.63,0.15,1];
ace_medical_gui_bloodLossColor_6 = [1,0.54,0.08,1];
ace_medical_gui_bloodLossColor_7 = [1,0.43,0.02,1];
ace_medical_gui_bloodLossColor_8 = [1,0.3,0,1];
ace_medical_gui_bloodLossColor_9 = [1,0,0,1];
ace_medical_gui_damageColor_0 = [1,1,1,1];
ace_medical_gui_damageColor_1 = [0.75,0.95,1,1];
ace_medical_gui_damageColor_2 = [0.62,0.86,1,1];
ace_medical_gui_damageColor_3 = [0.54,0.77,1,1];
ace_medical_gui_damageColor_4 = [0.48,0.67,1,1];
ace_medical_gui_damageColor_5 = [0.42,0.57,1,1];
ace_medical_gui_damageColor_6 = [0.37,0.47,1,1];
ace_medical_gui_damageColor_7 = [0.31,0.36,1,1];
ace_medical_gui_damageColor_8 = [0.22,0.23,1,1];
ace_medical_gui_damageColor_9 = [0,0,1,1];
force ace_medical_gui_enableActions = 1;
force ace_medical_gui_enableMedicalMenu = 1;
force ace_medical_gui_enableSelfActions = true;
force ace_medical_gui_interactionMenuShowTriage = 1;
force ace_medical_gui_maxDistance = 3;
force ace_medical_gui_openAfterTreatment = true;
force ace_medical_ivFlowRate = 1;
force ace_medical_limping = 1;
force ace_medical_painCoefficient = 1;
force ace_medical_painUnconsciousChance = 0.1;
force ace_medical_playerDamageThreshold = 5;
force ace_medical_spontaneousWakeUpChance = 0.15;
force ace_medical_spontaneousWakeUpEpinephrineBoost = 2.5;
force ace_medical_statemachine_AIUnconsciousness = true;
force ace_medical_statemachine_cardiacArrestBleedoutEnabled = true;
force ace_medical_statemachine_cardiacArrestTime = 300;
force ace_medical_statemachine_fatalInjuriesAI = 0;
force ace_medical_statemachine_fatalInjuriesPlayer = 0;
force ace_medical_treatment_advancedBandages = 2;
ace_medical_treatment_advancedDiagnose = 1;
force ace_medical_treatment_advancedMedication = true;
force ace_medical_treatment_allowBodyBagUnconscious = false;
force ace_medical_treatment_allowLitterCreation = true;
force ace_medical_treatment_allowSelfIV = 1;
force ace_medical_treatment_allowSelfPAK = 1;
force ace_medical_treatment_allowSelfStitch = 1;
force ace_medical_treatment_allowSharedEquipment = 1;
ace_medical_treatment_clearTrauma = 1;
force ace_medical_treatment_consumePAK = 1;
force ace_medical_treatment_consumeSurgicalKit = 0;
force ace_medical_treatment_convertItems = 0;
ace_medical_treatment_cprSuccessChanceMax = 0.4;
ace_medical_treatment_cprSuccessChanceMin = 0.4;
force ace_medical_treatment_holsterRequired = 3;
force ace_medical_treatment_litterCleanupDelay = 600;
force ace_medical_treatment_locationEpinephrine = 0;
force ace_medical_treatment_locationIV = 0;
force ace_medical_treatment_locationPAK = 0;
force ace_medical_treatment_locationsBoostTraining = false;
force ace_medical_treatment_locationSurgicalKit = 0;
force ace_medical_treatment_maxLitterObjects = 500;
force ace_medical_treatment_medicEpinephrine = 1;
force ace_medical_treatment_medicIV = 1;
force ace_medical_treatment_medicPAK = 1;
force ace_medical_treatment_medicSurgicalKit = 1;
force ace_medical_treatment_timeCoefficientPAK = 1;
force ace_medical_treatment_treatmentTimeAutoinjector = 5;
force ace_medical_treatment_treatmentTimeBodyBag = 15;
force ace_medical_treatment_treatmentTimeCPR = 15;
force ace_medical_treatment_treatmentTimeIV = 12;
force ace_medical_treatment_treatmentTimeSplint = 7;
force ace_medical_treatment_treatmentTimeTourniquet = 5;
force ace_medical_treatment_woundReopenChance = 2.00155;
force ace_medical_treatment_woundStitchTime = 5;

// ACE Name Tags
ace_nametags_ambientBrightnessAffectViewDist = 1;
ace_nametags_defaultNametagColor = [0.77,0.51,0.08,1];
ace_nametags_nametagColorBlue = [0.67,0.67,1,1];
ace_nametags_nametagColorGreen = [0.67,1,0.67,1];
ace_nametags_nametagColorMain = [1,1,1,1];
ace_nametags_nametagColorRed = [1,0.67,0.67,1];
ace_nametags_nametagColorYellow = [1,1,0.67,1];
force ace_nametags_playerNamesMaxAlpha = 0.8;
force ace_nametags_playerNamesViewDistance = 5;
force ace_nametags_showCursorTagForVehicles = false;
ace_nametags_showNamesForAI = false;
ace_nametags_showPlayerNames = 1;
ace_nametags_showPlayerRanks = true;
ace_nametags_showSoundWaves = 1;
ace_nametags_showVehicleCrewInfo = true;
ace_nametags_tagSize = 2;

// ACE Nightvision
force ace_nightvision_aimDownSightsBlur = 1;
force ace_nightvision_disableNVGsWithSights = false;
force ace_nightvision_effectScaling = 1;
force ace_nightvision_fogScaling = 1;
force ace_nightvision_noiseScaling = 1;
ace_nightvision_shutterEffects = true;

// ACE Overheating
ace_overheating_cookoffCoef = 1;
ace_overheating_coolingCoef = 1;
ace_overheating_displayTextOnJam = true;
force ace_overheating_enabled = true;
ace_overheating_heatCoef = 1;
ace_overheating_jamChanceCoef = 1;
force ace_overheating_overheatingDispersion = true;
ace_overheating_overheatingRateOfFire = true;
ace_overheating_particleEffectsAndDispersionDistance = 3000;
ace_overheating_showParticleEffects = true;
ace_overheating_showParticleEffectsForEveryone = false;
ace_overheating_suppressorCoef = 1;
force ace_overheating_unJamFailChance = 0.1;
force ace_overheating_unJamOnreload = false;
ace_overheating_unJamOnSwapBarrel = false;

// ACE Pointing
force ace_finger_enabled = true;
ace_finger_indicatorColor = [0.83,0.68,0.21,0.75];
ace_finger_indicatorForSelf = true;
force ace_finger_maxRange = 4;

// ACE Pylons
force ace_pylons_enabledForZeus = true;
force ace_pylons_enabledFromAmmoTrucks = true;
force ace_pylons_rearmNewPylons = false;
force ace_pylons_requireEngineer = false;
force ace_pylons_requireToolkit = true;
force ace_pylons_searchDistance = 15;
force ace_pylons_timePerPylon = 5;

// ACE Quick Mount
force ace_quickmount_distance = 3;
force ace_quickmount_enabled = true;
ace_quickmount_enableMenu = 3;
ace_quickmount_priority = 0;
force ace_quickmount_speed = 18;

// ACE Respawn
force ace_respawn_removeDeadBodiesDisconnected = true;
force ace_respawn_savePreDeathGear = false;

// ACE Scopes
force ace_scopes_correctZeroing = true;
force ace_scopes_deduceBarometricPressureFromTerrainAltitude = false;
force ace_scopes_defaultZeroRange = 100;
force ace_scopes_enabled = true;
force ace_scopes_forceUseOfAdjustmentTurrets = false;
force ace_scopes_overwriteZeroRange = false;
force ace_scopes_simplifiedZeroing = false;
ace_scopes_useLegacyUI = false;
force ace_scopes_zeroReferenceBarometricPressure = 1013.25;
force ace_scopes_zeroReferenceHumidity = 0;
force ace_scopes_zeroReferenceTemperature = 15;

// ACE Sitting
force acex_sitting_enable = true;

// ACE Spectator
force ace_spectator_enableAI = false;
ace_spectator_maxFollowDistance = 5;
force ace_spectator_restrictModes = 0;
force ace_spectator_restrictVisions = 0;

// ACE Switch Units
force ace_switchunits_enableSafeZone = true;
force ace_switchunits_enableSwitchUnits = false;
force ace_switchunits_safeZoneRadius = 100;
force ace_switchunits_switchToCivilian = false;
force ace_switchunits_switchToEast = false;
force ace_switchunits_switchToIndependent = false;
force ace_switchunits_switchToWest = false;

// ACE Trenches
force ace_trenches_bigEnvelopeDigDuration = 25;
force ace_trenches_bigEnvelopeRemoveDuration = 15;
force ace_trenches_smallEnvelopeDigDuration = 20;
force ace_trenches_smallEnvelopeRemoveDuration = 12;

// ACE Uncategorized
force ace_fastroping_requireRopeItems = false;
force ace_gunbag_swapGunbagEnabled = true;
force ace_hitreactions_minDamageToTrigger = 0.1;
ace_inventory_inventoryDisplaySize = 0;
force ace_laser_dispersionCount = 2;
force ace_microdagr_mapDataAvailable = 2;
force ace_microdagr_waypointPrecision = 3;
ace_optionsmenu_showNewsOnMainMenu = true;
force ace_overpressure_distanceCoefficient = 1;
ace_parachute_failureChance = 0;
ace_parachute_hideAltimeter = true;
ace_tagging_quickTag = 1;

// ACE User Interface
force ace_ui_allowSelectiveUI = true;
ace_ui_ammoCount = false;
ace_ui_ammoType = true;
ace_ui_commandMenu = true;
ace_ui_enableSpeedIndicator = true;
ace_ui_firingMode = true;
ace_ui_groupBar = false;
ace_ui_gunnerAmmoCount = true;
ace_ui_gunnerAmmoType = true;
ace_ui_gunnerFiringMode = true;
ace_ui_gunnerLaunchableCount = true;
ace_ui_gunnerLaunchableName = true;
ace_ui_gunnerMagCount = true;
ace_ui_gunnerWeaponLowerInfoBackground = true;
ace_ui_gunnerWeaponName = true;
ace_ui_gunnerWeaponNameBackground = true;
ace_ui_gunnerZeroing = true;
ace_ui_magCount = true;
ace_ui_soldierVehicleWeaponInfo = true;
ace_ui_staminaBar = true;
ace_ui_stance = true;
ace_ui_throwableCount = true;
ace_ui_throwableName = true;
ace_ui_vehicleAltitude = true;
ace_ui_vehicleCompass = true;
ace_ui_vehicleDamage = true;
ace_ui_vehicleFuelBar = true;
ace_ui_vehicleInfoBackground = true;
ace_ui_vehicleName = true;
ace_ui_vehicleNameBackground = true;
ace_ui_vehicleRadar = true;
ace_ui_vehicleSpeed = true;
ace_ui_weaponLowerInfoBackground = true;
ace_ui_weaponName = true;
ace_ui_weaponNameBackground = true;
ace_ui_zeroing = true;

// ACE Vehicle Lock
force ace_vehiclelock_defaultLockpickStrength = 10;
force ace_vehiclelock_lockVehicleInventory = false;
force ace_vehiclelock_vehicleStartingLockState = -1;

// ACE Vehicles
ace_vehicles_hideEjectAction = true;
force ace_vehicles_keepEngineRunning = false;
ace_vehicles_speedLimiterStep = 5;

// ACE View Distance Limiter
force ace_viewdistance_enabled = true;
force ace_viewdistance_limitViewDistance = 10000;
ace_viewdistance_objectViewDistanceCoeff = 0;
ace_viewdistance_viewDistanceAirVehicle = 0;
ace_viewdistance_viewDistanceLandVehicle = 0;
ace_viewdistance_viewDistanceOnFoot = 0;

// ACE View Restriction
force acex_viewrestriction_mode = 0;
force acex_viewrestriction_modeSelectiveAir = 0;
force acex_viewrestriction_modeSelectiveFoot = 0;
force acex_viewrestriction_modeSelectiveLand = 0;
force acex_viewrestriction_modeSelectiveSea = 0;
acex_viewrestriction_preserveView = false;

// ACE Volume
acex_volume_enabled = false;
acex_volume_fadeDelay = 1;
acex_volume_lowerInVehicles = false;
acex_volume_reduction = 5;
acex_volume_remindIfLowered = false;
acex_volume_showNotification = true;

// ACE Weapons
ace_common_persistentLaserEnabled = false;
force ace_laserpointer_enabled = true;
ace_reload_displayText = true;
ace_reload_showCheckAmmoSelf = false;
ace_weaponselect_displayText = true;

// ACE Weather
force ace_weather_enabled = true;
ace_weather_showCheckAirTemperature = true;
force ace_weather_updateInterval = 60;
force ace_weather_windSimulation = true;

// ACE Wind Deflection
force ace_winddeflection_enabled = true;
force ace_winddeflection_simulationInterval = 0.05;
force ace_winddeflection_vehicleEnabled = true;

// ACE Zeus
force ace_zeus_autoAddObjects = true;
force ace_zeus_canCreateZeus = 0;
force ace_zeus_radioOrdnance = false;
force ace_zeus_remoteWind = false;
force ace_zeus_revealMines = 0;
force ace_zeus_zeusAscension = false;
force ace_zeus_zeusBird = false;

// AH-64D Official Project
fza_ah64_aiFireControl = true;
fza_ah64_aiFireResponse = 15;
fza_ah64_aiFloodlight = false;
fza_ah64_enableClickHelper = true;
fza_ah64_headTrackAllowCursorMove = true;
fza_ah64_headTrackSensitivity = 0.5;
fza_ah64_sfmPlusStabilatorEnabled = 1;
fza_ah64_showPopup = true;
fza_ah64_vanillaTargetingEnable = true;

// AWESome Aerodynamics
orbis_aerodynamics_dynamicWindMode = 2;
force orbis_aerodynamics_enabled = true;
orbis_aerodynamics_fuelMassMultiplierGlobal = 1;
orbis_aerodynamics_pylonDragMultiplierGlobal = 1;
orbis_aerodynamics_pylonMassMultiplierGlobal = 1;
orbis_aerodynamics_windMultiplier = 1;

// AWESome ATC
orbis_atc_displayCallsign = 0;
orbis_atc_displayProjectileTrails = false;
orbis_atc_personalCallsign = "";
orbis_atc_radarTrailLength = 5;
orbis_atc_radarUpdateInterval = 0.5;
orbis_atc_unitSettingAlt = false;
orbis_atc_unitSettingHozGCI = false;
orbis_atc_unitSettingLatGCI = false;
orbis_atc_unitSettingSpd = false;
orbis_atc_updateIntervalATIS = 15;

// AWESome Cockpit
orbis_cockpit_checklistUnits = "KIAS";
orbis_cockpit_groundMultiplier = 1;
force orbis_cockpit_shakeEnabled = false;
orbis_cockpit_speedMultiplier = 1;

// AWESome GPWS
orbis_gpws_automaticTransponder = true;
orbis_gpws_defaultVolume = false;
orbis_gpws_personalDefault = "f16";

// BettIR
BettIR_ViewDistance = 300;

// Community Base Addons
cba_diagnostic_ConsoleIndentType = -1;
cba_disposable_dropUsedLauncher = 2;
force cba_disposable_replaceDisposableLauncher = true;
cba_events_repetitionMode = 1;
force cba_network_loadoutValidation = 0;
cba_optics_usePipOptics = true;
cba_ui_notifyLifetime = 4;
cba_ui_StorePasswords = 1;

// DUI - Squad Radar - Indicators
force diwako_dui_indicators_crew_range_enabled = false;
diwako_dui_indicators_fov_scale = false;
diwako_dui_indicators_icon_buddy = true;
diwako_dui_indicators_icon_leader = true;
diwako_dui_indicators_icon_medic = true;
diwako_dui_indicators_range = 20;
diwako_dui_indicators_range_crew = 300;
diwako_dui_indicators_range_scale = false;
diwako_dui_indicators_show = true;
diwako_dui_indicators_size = 1;
diwako_dui_indicators_style = "standard";
diwako_dui_indicators_useACENametagsRange = true;

// DUI - Squad Radar - Main
diwako_dui_ace_hide_interaction = true;
diwako_dui_colors = "standard";
diwako_dui_font = "RobotoCondensed";
diwako_dui_icon_style = "standard";
diwako_dui_main_hide_dialog = true;
diwako_dui_main_hide_ui_by_default = false;
diwako_dui_main_squadBlue = [0,0,1,1];
diwako_dui_main_squadGreen = [0,1,0,1];
diwako_dui_main_squadMain = [1,1,1,1];
diwako_dui_main_squadRed = [1,0,0,1];
diwako_dui_main_squadYellow = [1,1,0,1];
diwako_dui_main_trackingColor = [0.93,0.26,0.93,1];
diwako_dui_reset_ui_pos = false;

// DUI - Squad Radar - Nametags
diwako_dui_nametags_customRankStyle = "[[""PRIVATE"",""CORPORAL"",""SERGEANT"",""LIEUTENANT"",""CAPTAIN"",""MAJOR"",""COLONEL""],[""Pvt."",""Cpl."",""Sgt."",""Lt."",""Capt."",""Maj."",""Col.""]]";
diwako_dui_nametags_deadColor = [0.2,0.2,0.2,1];
diwako_dui_nametags_deadRenderDistance = 3.5;
diwako_dui_nametags_drawRank = true;
diwako_dui_nametags_enabled = true;
diwako_dui_nametags_enableFOVBoost = true;
diwako_dui_nametags_enableOcclusion = true;
diwako_dui_nametags_fadeInTime = 0.05;
diwako_dui_nametags_fadeOutTime = 0.5;
diwako_dui_nametags_fontGroup = "RobotoCondensedLight";
diwako_dui_nametags_fontGroupNameSize = 8;
diwako_dui_nametags_fontName = "RobotoCondensedBold";
diwako_dui_nametags_fontNameSize = 10;
diwako_dui_nametags_groupColor = [1,1,1,1];
diwako_dui_nametags_groupFontShadow = 1;
diwako_dui_nametags_groupNameOtherGroupColor = [0.6,0.85,0.6,1];
diwako_dui_nametags_nameFontShadow = 1;
diwako_dui_nametags_nameOtherGroupColor = [0.2,1,0,1];
diwako_dui_nametags_rankNameStyle = "default";
diwako_dui_nametags_renderDistance = 40;
diwako_dui_nametags_showUnconAsDead = true;
diwako_dui_nametags_useLIS = true;
diwako_dui_nametags_useSideIsFriendly = true;

// DUI - Squad Radar - Radar
diwako_dui_compass_hide_alone_group = false;
diwako_dui_compass_hide_blip_alone_group = false;
diwako_dui_compass_icon_scale = 1;
diwako_dui_compass_opacity = 1;
diwako_dui_compass_style = ["\z\diwako_dui\addons\radar\UI\compass_styles\standard\compass_limited.paa","\z\diwako_dui\addons\radar\UI\compass_styles\standard\compass.paa"];
diwako_dui_compassRange = 35;
diwako_dui_compassRefreshrate = 0;
diwako_dui_dir_showMildot = false;
diwako_dui_dir_size = 1.25;
diwako_dui_distanceWarning = 3;
diwako_dui_enable_compass = true;
diwako_dui_enable_compass_dir = 1;
diwako_dui_enable_occlusion = false;
diwako_dui_enable_occlusion_cone = 360;
diwako_dui_hudScaling = 1.33333;
diwako_dui_namelist = true;
diwako_dui_namelist_bg = 0;
diwako_dui_namelist_only_buddy_icon = false;
diwako_dui_namelist_size = 1.5396;
diwako_dui_namelist_text_shadow = 2;
diwako_dui_namelist_width = 215;
diwako_dui_radar_ace_finger = true;
force diwako_dui_radar_ace_medic = true;
diwako_dui_radar_compassRangeCrew = 500;
diwako_dui_radar_dir_padding = 25;
diwako_dui_radar_dir_shadow = 2;
diwako_dui_radar_group_by_vehicle = false;
diwako_dui_radar_icon_opacity = 1;
diwako_dui_radar_icon_opacity_no_player = true;
force diwako_dui_radar_icon_priority_setting = 1;
diwako_dui_radar_icon_scale_crew = 6;
diwako_dui_radar_leadingZeroes = false;
diwako_dui_radar_namelist_hideWhenLeader = false;
diwako_dui_radar_namelist_vertical_spacing = 0.75;
diwako_dui_radar_occlusion_fade_in_time = 1;
diwako_dui_radar_occlusion_fade_time = 10;
diwako_dui_radar_pointer_color = [1,0.5,0,1];
diwako_dui_radar_pointer_style = "standard";
diwako_dui_radar_show_cardinal_points = true;
diwako_dui_radar_showSpeaking = true;
diwako_dui_radar_showSpeaking_radioOnly = false;
diwako_dui_radar_showSpeaking_replaceIcon = true;
force diwako_dui_radar_sortType = "none";
force diwako_dui_radar_sqlFirst = false;
diwako_dui_radar_syncGroup = false;
force diwako_dui_radar_vehicleCompassEnabled = false;
diwako_dui_use_layout_editor = false;

// Enhanced Movement Rework
force emr_main_allowMidairClimbing = true;
force emr_main_animSpeedCoef = 1;
force emr_main_animSpeedStaminaCoef = 0.4;
force emr_main_blacklistStr = "";
force emr_main_climbingEnabled = true;
force emr_main_climbOnDuty = 3.4;
force emr_main_climbOverDuty = 3;
force emr_main_dropDuty = 0.7;
emr_main_dropViewElevation = -0.7;
emr_main_enableWalkableSurface = true;
force emr_main_enableWeightCheck = false;
emr_main_hintType = 2;
force emr_main_jumpDuty = 1;
force emr_main_jumpingEnabled = true;
force emr_main_jumpingLoadCoefficient = 1;
force emr_main_jumpVelocity = 3.4;
force emr_main_maxClimbHeight = 2.6;
force emr_main_maxDropHeight = 5;
force emr_main_maxWeightClimb1 = 100;
force emr_main_maxWeightClimb2 = 85;
force emr_main_maxWeightClimb3 = 60;
force emr_main_maxWeightJump = 100;
emr_main_preventHighVaulting = false;
force emr_main_staminaCoefficient = 1;
force emr_main_whitelistStr = "";

// Freestyle's Crash Landing
force fscl_captiveSystem = true;
force fscl_damageTreshold = 99;
force fscl_debug = false;
force fscl_ejectionProp = 33;
force fscl_ejectionSystem = true;
force fscl_gForceThreshold = 5;
force fscl_ignoreNonPlayerVehicles = true;
force fscl_stateThreshold = 14.95;

// GRAD Trenches
force grad_trenches_functions_allowBigEnvelope = true;
force grad_trenches_functions_allowCamouflage = true;
force grad_trenches_functions_allowDigging = true;
grad_trenches_functions_allowEffects = true;
force grad_trenches_functions_allowGiantEnvelope = true;
force grad_trenches_functions_allowHitDecay = true;
force grad_trenches_functions_allowLongEnvelope = true;
force grad_trenches_functions_allowShortEnvelope = true;
force grad_trenches_functions_allowSmallEnvelope = true;
force grad_trenches_functions_allowTrenchDecay = false;
force grad_trenches_functions_allowVehicleEnvelope = true;
force grad_trenches_functions_bigEnvelopeDamageMultiplier = 2;
force grad_trenches_functions_bigEnvelopeDigTime = 40;
force grad_trenches_functions_bigEnvelopeRemovalTime = -1;
force grad_trenches_functions_buildFatigueFactor = 1;
force grad_trenches_functions_camouflageRequireEntrenchmentTool = true;
force grad_trenches_functions_createTrenchMarker = false;
force grad_trenches_functions_decayTime = 1800;
force grad_trenches_functions_giantEnvelopeDamageMultiplier = 1;
force grad_trenches_functions_giantEnvelopeDigTime = 90;
force grad_trenches_functions_giantEnvelopeRemovalTime = -1;
force grad_trenches_functions_hitDecayMultiplier = 1;
force grad_trenches_functions_LongEnvelopeDigTime = 100;
force grad_trenches_functions_LongEnvelopeRemovalTime = -1;
force grad_trenches_functions_shortEnvelopeDamageMultiplier = 2;
force grad_trenches_functions_shortEnvelopeDigTime = 15;
force grad_trenches_functions_shortEnvelopeRemovalTime = -1;
force grad_trenches_functions_smallEnvelopeDamageMultiplier = 3;
force grad_trenches_functions_smallEnvelopeDigTime = 30;
force grad_trenches_functions_smallEnvelopeRemovalTime = -1;
force grad_trenches_functions_stopBuildingAtFatigueMax = true;
force grad_trenches_functions_timeoutToDecay = 7200;
force grad_trenches_functions_vehicleEnvelopeDamageMultiplier = 1;
force grad_trenches_functions_vehicleEnvelopeDigTime = 120;
force grad_trenches_functions_vehicleEnvelopeRemovalTime = -1;

// Headlamps
Headlamps_Settings_AllowAll = false;
Headlamps_Settings_ExtraCompatibleItems = "[]";

// LAMBS Danger
lambs_danger_cqbRange = 60;
lambs_danger_disableAIAutonomousManoeuvres = false;
lambs_danger_disableAIDeployStaticWeapons = false;
lambs_danger_disableAIFindStaticWeapons = false;
lambs_danger_disableAIHideFromTanksAndAircraft = false;
lambs_danger_disableAIPlayerGroup = false;
lambs_danger_disableAIPlayerGroupReaction = false;
lambs_danger_disableAutonomousFlares = false;
lambs_danger_disableAutonomousSmokeGrenades = false;
lambs_danger_panicChance = 0.1;

// LAMBS Danger Eventhandlers
lambs_eventhandlers_ExplosionEventHandlerEnabled = true;
lambs_eventhandlers_ExplosionReactionTime = 9;

// LAMBS Danger WP
lambs_wp_autoAddArtillery = false;

// LAMBS Main
lambs_main_combatShareRange = 200;
lambs_main_debug_drawAllUnitsInVehicles = false;
lambs_main_debug_Drawing = false;
lambs_main_debug_FSM = false;
lambs_main_debug_FSM_civ = false;
lambs_main_debug_functions = false;
lambs_main_debug_RenderExpectedDestination = false;
lambs_main_disableAICallouts = false;
lambs_main_disableAIDodge = false;
lambs_main_disableAIFleeing = false;
lambs_main_disableAIGestures = false;
lambs_main_disableAutonomousMunitionSwitching = false;
lambs_main_disablePlayerGroupSuppression = false;
lambs_main_indoorMove = 0.1;
lambs_main_maxRevealValue = 1;
lambs_main_minFriendlySuppressionDistance = 5;
lambs_main_minObstacleProximity = 5;
lambs_main_minSuppressionRange = 50;
lambs_main_radioBackpack = 2000;
lambs_main_radioDisabled = false;
lambs_main_radioEast = 500;
lambs_main_radioGuer = 500;
lambs_main_radioShout = 100;
lambs_main_radioWest = 500;

// MCC Classes
force MCC_convoyHVT = "[[""None"",""0""],[""B.Officer"",""B_officer_F""],[""B. Pilot"",""B_Helipilot_F""],[""O. Officer"",""O_officer_F""],[""O. Pilot"",""O_helipilot_F""],[""I.Commander"",""I_officer_F""],[""Citizen"",""C_man_polo_1_F""],[""C.Pilot"",""C_man_pilot_F""],[""Orestes"",""C_Orestes""],[""Nikos"",""C_Nikos""],[""Hunter"",""C_man_hunter_1_F""],[""Kerry"",""I_G_Story_Protagonist_F""]]";
force MCC_convoyHVTcar = "[[""Default"",""""],[""Hunter"",""B_Hunter_F""],[""MRAP"",""I_MRAP_03_F""],[""Quadbike"",""B_Quadbike_F""],[""Ifrit"",""O_Ifrit_F""],[""Offroad"",""C_Offroad_01_F""],[""SUV"",""C_SUV_01_F""],[""Hatchback"",""C_Hatchback_01_F""]]";
force MCC_ied_hidden = "[[""Dirt Small"",""IEDLandSmall_Remote_Ammo""],[""Dirt Big"",""IEDLandBig_Remote_Ammo""],[""Urban Small"",""IEDUrbanSmall_Remote_Ammo""],[""Urban Big"",""IEDUrbanBig_Remote_Ammo""]]";
force MCC_ied_medium = "[[""Wheel Cart"",""Land_WheelCart_F""],[""Metal Barrel"",""Land_MetalBarrel_F""],[""Plastic Barrel"",""Land_BarrelSand_F""],[""Pipes"",""Land_Pipes_small_F""],[""Wooden Crates"",""Land_CratesShabby_F""],[""Wooden Box"",""Land_WoodenBox_F""],[""Cinder Blocks"",""Land_Ytong_F""],[""Sacks Heap"",""Land_Sacks_heap_F""], [""Water Barrel"",""Land_WaterBarrel_F""],[""Water Tank"",""Land_WaterTank_F""]]";
force MCC_ied_small = "[[""Plastic Crates"",""Land_CratesPlastic_F""],[""Plastic Canister"",""Land_CanisterPlastic_F""],[""Sack"",""Land_Sack_F""],[""Road Cone"",""RoadCone""],[""Tyre"",""Land_Tyre_F""],[""Radio"",""Land_SurvivalRadio_F""],[""Suitcase"",""Land_Suitcase_F""],[""Grinder"",""Land_Grinder_F""],[""MultiMeter"",""Land_MultiMeter_F""],[""Plastic Bottle"",""Land_BottlePlastic_V1_F""],[""Fuel Canister"",""Land_CanisterFuel_F""],[""FM Radio"",""Land_FMradio_F""],[""Camera"",""Land_HandyCam_F""],[""Laptop"",""Land_Laptop_F""],[""Mobile Phone"",""Land_MobilePhone_old_F""],[""Smart Phone"",""Land_MobilePhone_smart_F""],[""Longrange Radio"",""Land_PortableLongRangeRadio_F""],[""Satellite Phone"",""Land_SatellitePhone_F""],[""Money"",""Land_Money_F""]]";
force MCC_ied_wrecks = "[[""MI-48"",""Land_UWreck_Heli_Attack_02_F""],[""BMP2"",""Land_Wreck_BMP2_F""],[""Car"",""Land_Wreck_Car_F""],[""Car2"",""Land_Wreck_Car2_F""],[""Car Dismantled"",""Land_Wreck_CarDismantled_F""],[""Blackfoot"",""Land_Wreck_Heli_Attack_01_F""],[""HMMW"",""Land_Wreck_HMMWV_F""],[""Hunter"",""Land_Wreck_Hunter_F""],[""Offroad2"",""Land_Wreck_Offroad2_F""],[""Skoda"",""Land_Wreck_Skodovka_F""],[""Slammer"",""Land_Wreck_Slammer_F""],[""T72"",""Land_Wreck_T72_hull_F""],[""Truck"",""Land_Wreck_Truck_dropside_F""],[""Truck2"",""Land_Wreck_Truck_F""],[""UAZ"",""Land_Wreck_UAZ_F""],[""Van"",""Land_Wreck_Van_F""],[""Car Wreck"",""Land_Wreck_Car3_F""],[""BRDM Wreck"",""Land_Wreck_BRDM2_F""],[""Offroad Wreck"",""Land_Wreck_Offroad_F""],[""Truck Wreck"",""Land_Wreck_Truck_FWreck""]]";
force MCC_initConstract_aa = "[""O_static_AA_F"",""B_static_AA_F"",""O_static_AA_F"",""O_static_AA_F""]";
force MCC_initConstract_at = "[""O_static_AT_F"",""B_static_AT_F"",""O_static_AT_F"",""O_static_AT_F""]";
force MCC_initConstract_bunker = "[""Land_BagBunker_Small_F"",""Land_BagBunker_Small_F"",""Land_BagBunker_Small_F"",""Land_BagBunker_Small_F""]";
force MCC_initConstract_gmg = "[""O_GMG_01_F"",""B_GMG_01_F"",""O_GMG_01_F"",""O_GMG_01_F""]";
force MCC_initConstract_gmgh = "[""O_GMG_01_high_F"",""B_GMG_01_high_F"",""O_GMG_01_high_F"",""O_GMG_01_high_F""]";
force MCC_initConstract_hmg = "[""O_HMG_01_F"",""B_HMG_01_F"",""O_HMG_01_F"",""O_HMG_01_F""]";
force MCC_initConstract_hmgh = "[""O_HMG_01_F"",""B_HMG_01_high_F"",""O_HMG_01_F"",""O_HMG_01_F""]";
force MCC_initConstract_mortar = "[""O_Mortar_01_F"",""B_Mortar_01_F"",""O_Mortar_01_F"",""O_Mortar_01_F""]";
force MCC_initConstract_wall = "[""Land_BagFence_Long_F"",""Land_HBarrier_3_F"",""Land_BagFence_Long_F"",""Land_BagFence_Long_F""]";

// MCC Commander Console
force MCC_allowConsole = true;
force MCC_allowRTS = true;
force MCC_ConsoleACTime = 300;
force MCC_ConsoleCanCommandAI = true;
force MCC_ConsoleLiveFeedHelmets = "[""H_HelmetB"",""H_HelmetB_paint"",""H_HelmetB_light"",""H_HelmetO_ocamo"",""H_HelmetLeaderO_ocamo"",""H_HelmetSpecO_ocamo"",""H_HelmetSpecO_blk""]";
force MCC_ConsoleLiveFeedHelmetsOnly = false;
force MCC_ConsoleOnlyShowUnitsWithGPS = true;
force MCC_ConsolePlayersCanSeeWPonMap = true;
force MCC_defaultCASEnabled = true;
force MCC_defaultSupplyDropsEnabled = true;

// MCC GAIA
force GAIA_CACHE_STAGE_1 = 3000;
force MCC_AI_AimIndex = 5;
force MCC_AI_CommandIndex = 5;
force MCC_AI_Skill_Index = 5;
force MCC_AI_SpotIndex = 5;
force MCC_GAIA_AMBIANT = false;
force MCC_GAIA_AMBIANT_CHANCE = 50;
force MCC_GAIA_ATTACKS_FOR_NONGAIA = false;

// MCC General
force MCC_allowlogistics = true;
force MCC_allowsqlPDA = true;
force MCC_ambientBirdsSettingIndex = true;
force MCC_ambientFireSettingIndex = false;
force MCC_armedCivilansWeapons = "[""hgun_P07_F"",""hgun_Rook40_F"",""hgun_ACPC2_F"",""hgun_Pistol_heavy_01_F"",""hgun_Pistol_heavy_02_F"",""SMG_01_F"",""SMG_02_F"",""hgun_PDW2000_F""]";
force MCC_artilleryComputerIndex = true;
force MCC_artillerySpreadArray = "[[""On-target"",0], [""Precise"",100], [""Tight"",200], [""Wide"",400]]";
MCC_CuratorEditDisabled = false;
force MCC_fnc_ambientFireInitCrewBurnChance = 50;
force MCC_fnc_ambientFireInitCrewNewFireChance = 10;
force MCC_fnc_ambientFireInitExplosivesBurnChance = 3;
force MCC_fnc_ambientFireInitVehicleBurnChance = 50;
force MCC_MessagesIndex = true;
force MCC_syncOn = true;
force MCC_t2tIndex = 1;
force MCC_timeMultiplier_settings = 1;

// MCC Mechanics
force MCC_allow3DSpotting = true;
force MCC_arcadeTanks = false;
force MCC_breacingAmmo = "[""prpl_8Rnd_12Gauge_Slug"",""prpl_6Rnd_12Gauge_Slug"",""rhsusf_8Rnd_Slug"",""rhsusf_5Rnd_Slug""]";
MCC_cover = false;
MCC_coverUI = false;
MCC_coverVault = false;
force MCC_disableFatigue = true;
force MCC_ingameUI = true;
force MCC_interaction = true;
force MCC_nonLeathalAmmo = "[""prpl_8Rnd_12Gauge_Slug"",""prpl_6Rnd_12Gauge_Slug"",""rhsusf_8Rnd_Slug"",""rhsusf_5Rnd_Slug""]";
MCC_quickWeaponChange = false;
force MCC_surviveModAllowSearch_index = 0;
force MCC_surviveModPlayerGear = false;
force MCC_surviveModPlayerPos = false;

// MCC Mission Generator
force MCC_MWAA = "[""B_APC_Tracked_01_AA_F"",""O_APC_Tracked_02_AA_F"",""I_APC_Wheeled_03_cannon_F""]";
force MCC_MWAir = "[""O_Heli_Attack_02_F"",""O_Heli_Attack_02_black_F"",""O_UAV_02_F"",""O_UAV_02_CAS_F"",""B_Heli_Attack_01_F"",""I_Plane_Fighter_03_CAS_F"",""I_Plane_Fighter_03_AA_F""]";
force MCC_MWArtillery = "[""B_MBT_01_arty_F"",""B_MBT_01_mlrs_F"",""O_MBT_02_arty_F"",""O_Mortar_01_F"",""I_Mortar_01_F""]";
force MCC_MWcache = "[""Box_East_AmmoVeh_F"",""Land_Pallet_MilBoxes_F""]";
force MCC_MWFuelTanks = "[""Land_dp_smallTank_F"",""Land_ReservoirTank_V1_F"",""Land_dp_bigTank_F""]";
force MCC_MWHVT = "[""B_officer_F"",""O_officer_F"",""I_officer_F"",""C_Nikos""]";
force MCC_MWIED = "[""IEDLandSmall_Remote_Ammo"",""IEDLandBig_Remote_Ammo"",""IEDUrbanSmall_Remote_Ammo"",""IEDUrbanBig_Remote_Ammo""]";
force MCC_MWIntelObjects = "[""Land_File2_F"",""Land_FilePhotos_F"",""Land_Laptop_unfolded_F"",""Land_SatellitePhone_F"",""Land_Suitcase_F""]";
force MCC_MWradar = "[""Land_Radar_Small_F"",""B_Radar_System_01_F"",""O_Radar_System_02_F""]";
force MCC_MWRadio = "[""Land_TTowerBig_2_F""]";
force MCC_MWSITES = "[[""Guerrilla"",""Camps"",""CampA""],[""Guerrilla"",""Camps"",""CampB""],[""Guerrilla"",""Camps"",""CampC""],[""Guerrilla"",""Camps"",""CampD""],[""Guerrilla"",""Camps"",""CampE""],[""Guerrilla"",""Camps"",""CampF""],[""MCC_comps"",""civilians"",""slums""],[""MCC_comps"",""Guerrilla"",""campSite""]]";
force MCC_MWSITESmilitary = "[[""Military"",""Outposts"",""OutpostA""],[""Military"",""Outposts"",""OutpostB""],[""Military"",""Outposts"",""OutpostC""],[""Military"",""Outposts"",""OutpostD""],[""Military"",""Outposts"",""OutpostE""],[""Military"",""Outposts"",""OutpostF""]]";
force MCC_MWTanks = "[""B_MBT_01_cannon_F"",""O_MBT_02_cannon_F""]";

// MCC Radio
force MCC_VonRadio = false;
force MCC_vonRadioDistanceCommander = 12000;
force MCC_vonRadioDistanceGlobal = 2000;
force MCC_vonRadioDistanceGroup = 30;
force MCC_vonRadioDistanceSide = 5000;
force MCC_vonRadioKickIdle = false;

// MCC Respawn
force MCC_deletePlayersBody = false;
force MCC_openRespawnMenu = true;
force MCC_respawnCinematic = true;
force MCC_respawnOnGroupLeader = false;
force MCC_saveGearIndex = false;

// MCC Role Selection
force CP_activated = false;
force CP_defaultGroups = "[""Alpha"",""Bravo"",""Charlie"",""Delta""]";
force CP_expNotifications = true;
force CP_XPperLevel = 4000;
force MCC_allowChangingKits = true;
force MCC_rsAllWeapons = false;
force MCC_rsEnableDriversPilots = false;
force MCC_rsEnableRoleWeapons = false;

// MCC User Interface
force MCC_groupMarkers = false;
force MCC_hitRadar = 0;
force MCC_indevidualMarkers = false;
MCC_nameTags = false;
MCC_NameTags_direct = false;
force MCC_Settings_compassEnabled = 2;
force MCC_Settings_forceCamera = 3;
MCC_Settings_grassDetail = 10;
MCC_Settings_ViewDistance = 5000;
force MCC_showActionKey = true;
force MCC_suppressionOn = false;
force MCC_UIModuleTickets = false;

// NIArms
niarms_gripSwitch = true;
force niarms_magSwitch = true;

// TFAR - Clientside settings
TFAR_curatorCamEars = false;
TFAR_default_radioVolume = 6;
TFAR_intercomDucking = 0.2;
TFAR_intercomVolume = 0.1;
TFAR_moveWhileTabbedOut = false;
TFAR_noAutomoveSpectator = false;
TFAR_oldVolumeHint = false;
TFAR_pluginTimeout = 4;
TFAR_PosUpdateMode = 0.1;
TFAR_showChannelChangedHint = true;
TFAR_ShowDiaryRecord = true;
TFAR_showTransmittingHint = true;
TFAR_ShowVolumeHUD = false;
TFAR_tangentReleaseDelay = 0;
TFAR_VolumeHudTransparency = 0;
TFAR_volumeModifier_forceSpeech = false;

// TFAR - Global settings
force TFAR_AICanHearPlayer = true;
force TFAR_AICanHearSpeaker = true;
force TFAR_allowDebugging = true;
tfar_core_noTSNotConnectedHint = false;
force TFAR_defaultIntercomSlot = 0;
force TFAR_disableAutoMute = false;
force TFAR_enableIntercom = true;
force TFAR_experimentalVehicleIsolation = true;
force TFAR_fullDuplex = true;
force TFAR_giveLongRangeRadioToGroupLeaders = false;
force TFAR_giveMicroDagrToSoldier = false;
force TFAR_givePersonalRadioToRegularSoldier = true;
force TFAR_globalRadioRangeCoef = 1;
force TFAR_instantiate_instantiateAtBriefing = false;
force TFAR_objectInterceptionEnabled = true;
force TFAR_objectInterceptionStrength = 400;
force tfar_radiocode_east = "_opfor";
force tfar_radiocode_independent = "_independent";
force tfar_radiocode_west = "_bluefor";
force tfar_radioCodesDisabled = false;
force TFAR_SameLRFrequenciesForSide = true;
force TFAR_SameSRFrequenciesForSide = false;
force TFAR_setting_defaultFrequencies_lr_east = "";
force TFAR_setting_defaultFrequencies_lr_independent = "";
force TFAR_setting_defaultFrequencies_lr_west = "40.1,41.1,42.1,43.1,44.1,45.1,46.1";
force TFAR_setting_defaultFrequencies_sr_east = "";
force TFAR_setting_defaultFrequencies_sr_independent = "";
force TFAR_setting_defaultFrequencies_sr_west = "130.1,131.1,132.1,133.1,134.1,135.1,136.1,137.1,138.1";
force TFAR_setting_DefaultRadio_Airborne_east = "TFAR_mr6000l";
force TFAR_setting_DefaultRadio_Airborne_Independent = "TFAR_anarc164";
force TFAR_setting_DefaultRadio_Airborne_West = "TFAR_anarc210";
force TFAR_setting_DefaultRadio_Backpack_east = "TFAR_mr3000";
force TFAR_setting_DefaultRadio_Backpack_Independent = "TFAR_anprc155";
force TFAR_setting_DefaultRadio_Backpack_west = "TFAR_rt1523g";
force TFAR_setting_DefaultRadio_Personal_east = "TFAR_fadak";
force TFAR_setting_DefaultRadio_Personal_Independent = "TFAR_anprc148jem";
force TFAR_setting_DefaultRadio_Personal_West = "TFAR_anprc152";
force TFAR_setting_DefaultRadio_Rifleman_East = "TFAR_pnr1000a";
force TFAR_setting_DefaultRadio_Rifleman_Independent = "TFAR_anprc154";
force TFAR_setting_DefaultRadio_Rifleman_West = "TFAR_anprc152";
force TFAR_spectatorCanHearEnemyUnits = true;
force TFAR_spectatorCanHearFriendlies = true;
force TFAR_takingRadio = 2;
force TFAR_Teamspeak_Channel_Name = "TaskForceRadio";
force TFAR_Teamspeak_Channel_Password = "123";
force tfar_terrain_interception_coefficient = 7;
force TFAR_voiceCone = true;

// UH-60M
vtx_uh60_anvishud_defaultMode = -1;
vtx_uh60_anvishud_settingBrightness = 0.6;
vtx_uh60_anvishud_settingColor = [0.082,0.608,0.039];
vtx_uh60_flir_setting_AimSlewBlockMouse = true;
vtx_uh60_flir_setting_AimSlewSpeed = 1;
vtx_uh60_flir_setting_AimXFactor = 1;
vtx_uh60_flir_setting_animateTurret = true;
vtx_uh60_flir_setting_KeySlewSpeed = 1;
vtx_uh60_flir_setting_KeyXFactor = 1;
vtx_uh60_flir_setting_syncDelay = 0.015;
vtx_uh60_ui_showDebugMessages = true;
vtx_uh60m_enabled_waypts = false;
vtx_uh60m_simpleCollective = false;
force vtx_uh60m_simpleStartup = false;
vtx_uh60m_trackIR_interaction_cursor = false;
vtx_uh60m_trackIR_interaction_cursorSensitivity = 2.5;

// VXF Interaction
force vtx_ace_viv_loadDistance = 15;
force vtx_ace_viv_timeFactor = 1;

// Zeus Enhanced
zen_camera_adaptiveSpeed = true;
zen_camera_defaultSpeedCoef = 1;
zen_camera_fastSpeedCoef = 1;
zen_camera_followTerrain = true;
force zen_common_ascensionMessages = false;
force zen_common_autoAddObjects = false;
force zen_common_cameraBird = false;
zen_common_darkMode = false;
zen_common_disableGearAnim = false;
zen_common_preferredArsenal = 1;
zen_compat_ace_hideModules = true;
zen_context_menu_enabled = 2;
zen_context_menu_overrideWaypoints = false;
zen_editor_addGroupIcons = false;
zen_editor_declutterEmptyTree = true;
zen_editor_disableLiveSearch = false;
zen_editor_moveDisplayToEdge = true;
force zen_editor_parachuteSounds = true;
zen_editor_previews_enabled = true;
zen_editor_randomizeCopyPaste = false;
zen_editor_removeWatermark = true;
zen_editor_unitRadioMessages = 0;
zen_placement_enabled = false;
zen_remote_control_cameraExitPosition = 2;
zen_visibility_enabled = false;
zen_vision_enableBlackHot = false;
zen_vision_enableBlackHotGreenCold = false;
zen_vision_enableBlackHotRedCold = false;
zen_vision_enableGreenHotCold = false;
zen_vision_enableNVG = true;
zen_vision_enableRedGreenThermal = false;
zen_vision_enableRedHotCold = false;
zen_vision_enableWhiteHot = true;
zen_vision_enableWhiteHotRedCold = false;

// Zeus Enhanced - Faction Filter
zen_faction_filter_0_FIR_AWS_ENEMY_F = true;
zen_faction_filter_0_LOP_AFR_OPF = true;
zen_faction_filter_0_LOP_AM_OPF = true;
zen_faction_filter_0_LOP_BH = true;
zen_faction_filter_0_LOP_ChDKZ = true;
zen_faction_filter_0_LOP_IRA = true;
zen_faction_filter_0_LOP_ISTS_OPF = true;
zen_faction_filter_0_LOP_NK = true;
zen_faction_filter_0_LOP_SLA = true;
zen_faction_filter_0_LOP_SYR = true;
zen_faction_filter_0_LOP_TKA = true;
zen_faction_filter_0_LOP_US = true;
zen_faction_filter_0_OPF_BlackOrder_F = true;
zen_faction_filter_0_OPF_BlackOrder_MP_F = true;
zen_faction_filter_0_OPF_BlackOrder_SCI_F = true;
zen_faction_filter_0_OPF_F = true;
zen_faction_filter_0_OPF_G_F = true;
zen_faction_filter_0_OPF_GEN_F = true;
zen_faction_filter_0_OPF_R_F = true;
zen_faction_filter_0_OPF_T_F = true;
zen_faction_filter_0_rhs_faction_msv = true;
zen_faction_filter_0_rhs_faction_rva = true;
zen_faction_filter_0_rhs_faction_tv = true;
zen_faction_filter_0_rhs_faction_vdv = true;
zen_faction_filter_0_rhs_faction_vmf = true;
zen_faction_filter_0_rhs_faction_vpvo = true;
zen_faction_filter_0_rhs_faction_vv = true;
zen_faction_filter_0_rhs_faction_vvs = true;
zen_faction_filter_0_rhs_faction_vvs_c = true;
zen_faction_filter_0_rhsgref_faction_chdkz = true;
zen_faction_filter_0_rhsgref_faction_chdkz_groups = true;
zen_faction_filter_0_rhsgref_faction_tla = true;
zen_faction_filter_0_rhssaf_faction_airforce_opfor = true;
zen_faction_filter_0_rhssaf_faction_army_opfor = true;
zen_faction_filter_1_ = true;
zen_faction_filter_1_BLU_CTRG_F = true;
zen_faction_filter_1_BLU_F = true;
zen_faction_filter_1_BLU_G_F = true;
zen_faction_filter_1_BLU_GEN_F = true;
zen_faction_filter_1_BLU_T_F = true;
zen_faction_filter_1_BLU_W_F = true;
zen_faction_filter_1_fza_usaav = true;
zen_faction_filter_1_ION = true;
zen_faction_filter_1_ION_Choppers = true;
zen_faction_filter_1_LOP_AA = true;
zen_faction_filter_1_LOP_CDF = true;
zen_faction_filter_1_LOP_GRE = true;
zen_faction_filter_1_LOP_IA = true;
zen_faction_filter_1_LOP_PESH = true;
zen_faction_filter_1_rhs_faction_socom = true;
zen_faction_filter_1_rhs_faction_usaf = true;
zen_faction_filter_1_rhs_faction_usarmy_d = true;
zen_faction_filter_1_rhs_faction_usarmy_wd = true;
zen_faction_filter_1_rhs_faction_usmc_d = true;
zen_faction_filter_1_rhs_faction_usmc_wd = true;
zen_faction_filter_1_rhs_faction_usn = true;
zen_faction_filter_1_rhsgref_faction_cdf_air_b = true;
zen_faction_filter_1_rhsgref_faction_cdf_ground_b = true;
zen_faction_filter_1_rhsgref_faction_cdf_ground_b_groups = true;
zen_faction_filter_1_rhsgref_faction_cdf_ng_b = true;
zen_faction_filter_1_rhsgref_faction_hidf = true;
zen_faction_filter_1_SPAR_Air_Force = true;
zen_faction_filter_1_SPAR_Special_Ops = true;
zen_faction_filter_1_SPAR_Special_Ops_A = true;
zen_faction_filter_1_SPAR_Special_Ops_D = true;
zen_faction_filter_1_SPAR_Task_Force = true;
zen_faction_filter_1_SPAR_Task_Force_WD = true;
zen_faction_filter_1_VSM_GP = true;
zen_faction_filter_2_IND_BlackOrder_F = true;
zen_faction_filter_2_IND_BlackOrder_MP_F = true;
zen_faction_filter_2_IND_BlackOrder_SCI_F = true;
zen_faction_filter_2_IND_C_F = true;
zen_faction_filter_2_IND_E_F = true;
zen_faction_filter_2_IND_F = true;
zen_faction_filter_2_IND_G_F = true;
zen_faction_filter_2_IND_L_F = true;
zen_faction_filter_2_LOP_AFR = true;
zen_faction_filter_2_LOP_AM = true;
zen_faction_filter_2_LOP_IRAN = true;
zen_faction_filter_2_LOP_ISTS = true;
zen_faction_filter_2_LOP_NAPA = true;
zen_faction_filter_2_LOP_PESH_IND = true;
zen_faction_filter_2_LOP_PMC = true;
zen_faction_filter_2_LOP_RACS = true;
zen_faction_filter_2_LOP_TRK = true;
zen_faction_filter_2_LOP_UA = true;
zen_faction_filter_2_LOP_UKR = true;
zen_faction_filter_2_LOP_UN = true;
zen_faction_filter_2_LOP_UVF = true;
zen_faction_filter_2_rhsgref_faction_cdf_air = true;
zen_faction_filter_2_rhsgref_faction_cdf_ground = true;
zen_faction_filter_2_rhsgref_faction_cdf_ground_groups = true;
zen_faction_filter_2_rhsgref_faction_cdf_ng = true;
zen_faction_filter_2_rhsgref_faction_cdf_ng_groups = true;
zen_faction_filter_2_rhsgref_faction_chdkz_g = true;
zen_faction_filter_2_rhsgref_faction_chdkz_g_groups = true;
zen_faction_filter_2_rhsgref_faction_nationalist = true;
zen_faction_filter_2_rhsgref_faction_nationalist_groups = true;
zen_faction_filter_2_rhsgref_faction_tla_g = true;
zen_faction_filter_2_rhsgref_faction_un = true;
zen_faction_filter_2_rhssaf_faction_airforce = true;
zen_faction_filter_2_rhssaf_faction_army = true;
zen_faction_filter_2_rhssaf_faction_un = true;
zen_faction_filter_3_CIV_F = true;
zen_faction_filter_3_CIV_IDAP_F = true;
zen_faction_filter_3_EdCat_Things = true;
zen_faction_filter_3_IND_L_F = true;
zen_faction_filter_3_LOP_AFR_Civ = true;
zen_faction_filter_3_LOP_CHR_Civ = true;
zen_faction_filter_3_LOP_TAK_Civ = true;
```